
save:
	cp ~/.vimrc vimrc
	cp ~/.zshrc zshrc
	cp ~/.gitconfig gitconfig
	cp ~/.spacemacs spacemacs
	cp ~/.tmux.conf tmux.conf
	cp ~/.zathurarc zathurarc
	cp ~/.alacritty.yml alacritty.yml
	cp ~/.xmonad/xmonad.hs xmonad.hs

install:
	cp vimrc ~/.vimrc
	cp zshrc ~/.zshrc
	cp gitconfig ~/.gitconfig
	cp spacemacs ~/.spacemacs
	cp tmux.conf ~/.tmux.conf
	cp zathurarc ~/.zathurarc
	cp alacritty.yml ~/.alacritty.yml
	cp xmonad.hs ~/.xmonad/xmonad.hs

push:
	git a
	git commit -m "addition to dotfiles"
	git push origin master
