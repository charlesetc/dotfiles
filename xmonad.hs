import XMonad hiding (Tall)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Actions.CycleWS
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.LayoutHints
import XMonad.Layout.Spiral
import XMonad.Layout.Reflect
import XMonad.Layout.ResizableTile
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizeScreen
import XMonad.Layout.Spacing
import XMonad.Layout.MultiToggle
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Util.Run(spawnPipe)

import System.Exit
import System.IO
import Data.Monoid

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ withUrgencyHook NoUrgencyHook
           $ defaultConfig {
                terminal           = "alacritty",
                -- modMask            = mod4Mask,
                workspaces         = ["1", "2", "3", "4", "5"],
                borderWidth        = 6,
                normalBorderColor  = "#000000",
                -- focusedBorderColor = "#b889ff",
                focusedBorderColor = "#4c63ff",
                -- focusedBorderColor = "#3399cc",
                -- focusedBorderColor = "#000000",
                manageHook         = myManageHook,
                keys               = myKeys,
                mouseBindings      = myMouseBindings,
                layoutHook         = myLayout,
                logHook            = dynamicLogWithPP $ xmobarPP {
                                         ppOutput = hPutStrLn xmproc,
                                         ppTitle = xmobarColor "green" "" . shorten 50
                                     }
    }

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $

    [ ((modMask              , xK_t   ), spawn $ XMonad.terminal conf)
    -- , ((modMask              , xK_Up   ), spawn $ XMonad.terminal conf)
    , ((modMask .|. controlMask, xK_w        ), kill)
    , ((modMask              , xK_Return    ), sendMessage NextLayout)
    , ((modMask .|. shiftMask, xK_Return    ), setLayout $ XMonad.layoutHook conf)
    -- , ((modMask              , xK_n        ), refresh)
    , ((modMask              , xK_n        ), sendMessage (Toggle REFLECTX))
    , ((modMask              , xK_e        ), sendMessage (Toggle REFLECTY))
    , ((modMask              , xK_Tab      ), windows W.focusDown)
    , ((modMask              , xK_Right      ), windows W.focusDown)
    , ((modMask              , xK_j        ), windows W.focusDown)
    , ((modMask              , xK_k        ), windows W.focusUp)
    , ((modMask              , xK_m        ), windows W.focusMaster)
    , ((modMask, xK_BackSpace   ), windows W.swapMaster)
    , ((modMask .|. controlMask, xK_j        ), windows W.swapDown)
    , ((modMask .|. controlMask, xK_k        ), windows W.swapUp)
    , ((modMask              , xK_h        ), sendMessage Shrink)
    , ((modMask              , xK_l        ), sendMessage Expand)
    , ((modMask .|. controlMask, xK_h        ), sendMessage MirrorShrink)
    , ((modMask .|. controlMask, xK_l        ), sendMessage MirrorExpand)
    , ((modMask              , xK_f        ), spawn "firefox")
    , ((modMask              , xK_Up        ), spawn "alacritty -e zsh -c '. ~/.zshrc; vim'")
    , ((modMask              , xK_c        ), spawn "xclock -digital -brief -twelve -padding 80 -face 'courier-24:bold'")
    , ((modMask              , xK_g        ), withFocused $ windows . W.sink)
    , ((modMask              , xK_comma    ), sendMessage (IncMasterN 1))
    , ((modMask              , xK_period   ), sendMessage (IncMasterN (-1)))
    , ((modMask .|. shiftMask, xK_q        ), io (exitWith ExitSuccess))
    , ((modMask              , xK_q        ), spawn "xmonad --recompile; xmonad --restart")
    , ((modMask              , xK_F2       ), shellPrompt defaultXPConfig)

    , ((modMask, xK_Left),  sendMessage NextLayout)
    , ((modMask, xK_Down),  windows W.swapMaster)

    , ((modMask, xK_r),  nextWS)
    , ((modMask, xK_s),    prevWS)
    , ((modMask .|. controlMask, xK_r), shiftToNext >> nextWS)
    , ((modMask .|. controlMask, xK_s),   shiftToPrev >> prevWS)

    , ((0                    , 0x1008ff13  ), spawn "amixer -q set Master 2dB+")
    , ((0                    , 0x1008ff11  ), spawn "amixer -q set Master 2dB-")
    , ((0                    , 0x1008ff12  ), spawn "amixer -q set Master toggle")
    , ((0                    , 0x1008ff16  ), spawn "mocp --prev")
    , ((0                    , 0x1008ff17  ), spawn "mocp --next")
    , ((0                    , 0x1008ff14  ), spawn "mocp --toggle-pause")
    , ((modMask              , xK_p    ), spawn "scrot --select")
    ]
    ++

    [((m .|. modMask, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_5]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

    ++

    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_z, xK_o, xK_a] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask, button1), (\w -> focus w >> mouseMoveWindow w
                                          >> windows W.shiftMaster))
    , ((modMask, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modMask, button3), (\w -> focus w >> mouseResizeWindow w
                                          >> windows W.shiftMaster))
    ]

myLayout =
    mkToggle (single REFLECTY) $
    mkToggle (single REFLECTX) $
    noOutsideBorders (space (spiral (4/3))) ||| noBorders Full
  where
     n = 18
     space = spacing (n - 6)
     noOutsideBorders = resizeHorizontal (n) . resizeVertical (n) .
                        resizeHorizontalRight (n) . resizeVerticalBottom (n)
     -- tall = ResizableTall nmaster delta ratio []
     -- nmaster = 1
     -- delta   = 3/100
     -- ratio   = 1/2

myManageHook :: ManageHook
myManageHook = composeAll
    [ className =? "Pidgin"    --> doShift "1:Chat"
    , className =? "Shiretoko" --> doShift "2:Web"]
